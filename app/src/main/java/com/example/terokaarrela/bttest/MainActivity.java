package com.example.terokaarrela.bttest;

import android.Manifest;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothManager;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Handler;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    private static final String TAG = "MainActivity";
    private Button btnScan;
    private ListView peripheralsListView;
    private BluetoothLeService bluetoothLeService;
    private BluetoothAdapter mBluetoothAdapter;
    private boolean mScanning;
    private Handler mHandler;
    private static final long SCAN_PERIOD = 2000;
    private List<BluetoothDevice> btDevices;
    private ArrayAdapter<BluetoothDevice> peripheralAdapter;
    private static final int MY_PERMISSIONS_REQUEST_COARSE_LOCATION=1;
    private TextView txtProgress;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        final BluetoothManager bluetoothManager = (BluetoothManager) getSystemService(Context.BLUETOOTH_SERVICE);
        this.mBluetoothAdapter = bluetoothManager.getAdapter();
        this.mHandler = new Handler();
        this.txtProgress = (TextView) findViewById(R.id.txtProgress);
        this.btnScan = (Button) findViewById(R.id.btnScan);
        this.btnScan.setOnClickListener(this);
        this.btDevices = new ArrayList<>();
        this.peripheralsListView = (ListView) findViewById(R.id.peripheralsListView);
        this.peripheralAdapter = new ArrayAdapter<BluetoothDevice>(this,android.R.layout.simple_expandable_list_item_1, btDevices);
        this.peripheralsListView.setAdapter(peripheralAdapter);
        final Intent intent = new Intent(this, PeripheralActivity.class);
        this.peripheralsListView.setOnItemClickListener(new AdapterView.OnItemClickListener()
        {
            public void onItemClick(AdapterView<?> arg0, View v,int position, long arg3)
            {
                BluetoothDevice selectedDevice =btDevices.get(position);
                //Toast.makeText(getApplicationContext(), "Peripheral Selected : "+selectedDevice.getAddress(), Toast.LENGTH_SHORT).show();
                intent.putExtra("PERIPHERAL_ADDRESS", selectedDevice.getAddress());
                startActivity(intent);
            }
        });

        // Here, thisActivity is the current activity
        if (ContextCompat.checkSelfPermission(this,
                Manifest.permission.ACCESS_COARSE_LOCATION)
                != PackageManager.PERMISSION_GRANTED) {

            // Should we show an explanation?
            if (ActivityCompat.shouldShowRequestPermissionRationale(this,
                    Manifest.permission.ACCESS_COARSE_LOCATION)) {

                // Show an explanation to the user *asynchronously* -- don't block
                // this thread waiting for the user's response! After the user
                // sees the explanation, try again to request the permission.

            } else {

                // No explanation needed, we can request the permission.

                ActivityCompat.requestPermissions(this,
                        new String[]{Manifest.permission.ACCESS_COARSE_LOCATION},
                        MY_PERMISSIONS_REQUEST_COARSE_LOCATION);

                // MY_PERMISSIONS_REQUEST_READ_CONTACTS is an
                // app-defined int constant. The callback method gets the
                // result of the request.
            }
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           String permissions[], int[] grantResults) {
        switch (requestCode) {
            case MY_PERMISSIONS_REQUEST_COARSE_LOCATION: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                    // permission was granted, yay! Do the
                    // contacts-related task you need to do.
                    Log.i(TAG, "Permissions granted");

                } else {
                    Log.i(TAG, "Permissions denied!");

                    // permission denied, boo! Disable the
                    // functionality that depends on this permission.
                }
                return;
            }

            // other 'case' lines to check for other
            // permissions this app might request
        }
    }

    @Override
    public void onClick(View view) {
        Log.i(TAG, "Click " + view.getId());
        switch (view.getId()){
            case R.id.btnScan:
                this.scanLeDevice(true);
                break;
            default:
                System.out.println("Click listener not implemented!");
                break;
        }
    }

    private void scanLeDevice(final boolean enable) {
        btDevices.clear();
        peripheralAdapter.notifyDataSetChanged();
        Log.i(TAG, "Start scanning: " + enable);
        txtProgress.setText("Scanning....");
        if (enable) {
            // Stops scanning after a pre-defined scan period.
            this.mHandler.postDelayed(new Runnable() {
                @Override
                public void run() {
                    mScanning = false;
                    mBluetoothAdapter.stopLeScan(mLeScanCallback);
                    Log.i(TAG, "==> Stopped le scan");
                    txtProgress.setText("");
                }
            }, SCAN_PERIOD);

            mScanning = true;
            UUID[] services = new UUID[]{BluetoothLeService.UUID_CUSTOM_SERVICE};
            mBluetoothAdapter.startLeScan(services,mLeScanCallback);
        } else {
            mScanning = false;
            mBluetoothAdapter.stopLeScan(mLeScanCallback);
        }

    }

    private BluetoothAdapter.LeScanCallback mLeScanCallback = new BluetoothAdapter.LeScanCallback() {

        @Override
        public void onLeScan(final BluetoothDevice device, int rssi, byte[] scanRecord) {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    boolean deviceExists = false;
                    for (int i = 0; i < btDevices.size(); i++) {
                        if(btDevices.get(i).getAddress().equals(device.getAddress())){
                            deviceExists = true;
                            break;
                        }
                    }
                    if(!deviceExists) {
                        btDevices.add(device);
                        peripheralAdapter.notifyDataSetChanged();
                    }
                }
            });
        }
    };

}
