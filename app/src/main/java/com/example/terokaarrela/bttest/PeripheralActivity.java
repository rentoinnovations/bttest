package com.example.terokaarrela.bttest;

import android.bluetooth.BluetoothGattCharacteristic;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.ServiceConnection;
import android.graphics.Color;
import android.os.IBinder;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.ViewStub;
import android.widget.Button;
import android.widget.TextView;

public class PeripheralActivity extends AppCompatActivity implements View.OnClickListener {

    private BluetoothLeService bluetoothLeService;
    private final static String TAG = "PeripheralActivity";
    private TextView mConnectionState;
    private TextView txtData;
    private String peripheralAddress;
    private boolean mBluetoothLeServiceBound = false;
    private Button btnConnect;
    private Button btnDisconnect;
    private Button btnSwStatus;
    private Button btnDeviceType;
    private Button btnWrite;
    private boolean mConnected;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_peripheral);
        Intent intent = getIntent();
        String address = intent.getStringExtra("PERIPHERAL_ADDRESS");

        this.mConnected = false;

        // Capture the layout's TextView and set the string as its text
        this.mConnectionState = (TextView) findViewById(R.id.txtProgress);
        this.mConnectionState.setText("");
        this.txtData = (TextView) findViewById(R.id.txtData);
        this.btnConnect = (Button) findViewById(R.id.btnConnect);
        this.btnConnect.setOnClickListener(this);
        this.btnDisconnect = (Button) findViewById(R.id.btnDisconnect);
        this.btnDisconnect.setOnClickListener(this);
        this.btnSwStatus = (Button) findViewById(R.id.btnReadSwStatus);
        this.btnSwStatus.setOnClickListener(this);
        this.btnSwStatus.setVisibility(View.INVISIBLE);
        this.btnDeviceType = (Button) findViewById(R.id.btnReadDeviceType);
        this.btnDeviceType.setOnClickListener(this);
        this.btnDeviceType.setVisibility(View.INVISIBLE);

        this.btnWrite = (Button) findViewById(R.id.btnWrite);
        this.btnWrite.setOnClickListener(this);
        this.btnWrite.setVisibility(View.INVISIBLE);

        this.peripheralAddress = address;
        Intent gattServiceIntent = new Intent(this, BluetoothLeService.class);
        bindService(gattServiceIntent, mServiceConnection, BIND_AUTO_CREATE);
        this.setTitle("Device: " + this.peripheralAddress);
        registerReceiver(mGattUpdateReceiver, makeGattUpdateIntentFilter());
    }

    /** Defines callbacks for service binding, passed to bindService() */
    private ServiceConnection mServiceConnection = new ServiceConnection() {

        @Override
        public void onServiceConnected(ComponentName componentName, IBinder service) {
            // We've bound to LocalService, cast the IBinder and get LocalService instance
            bluetoothLeService = ((BluetoothLeService.LocalBinder) service).getService();
            if (!bluetoothLeService.initialize()) {
                Log.e(TAG, "Unable to initialize Bluetooth");
                finish();
            }
            mBluetoothLeServiceBound = true;
        }

        @Override
        public void onServiceDisconnected(ComponentName componentName) {
            Log.i(TAG, "SERVICE DISCONNECTED");
            mBluetoothLeServiceBound = false;
            bluetoothLeService = null;
        }
    };


    private final BroadcastReceiver mGattUpdateReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            final String action = intent.getAction();
            if (BluetoothLeService.ACTION_GATT_CONNECTED.equals(action)) {
                updateConnectedState();
            } else if (BluetoothLeService.ACTION_GATT_DISCONNECTED.equals(action)) {
                updateDiscconnectedState();
            } else if (BluetoothLeService.ACTION_GATT_SERVICES_DISCOVERED.equals(action)) {
                Log.i(TAG, "Services discovered:" + bluetoothLeService.getSupportedGattServices().size());
                displayData(bluetoothLeService.getSupportedGattServices().size() + " services found.");
            } else if (BluetoothLeService.ACTION_DATA_AVAILABLE.equals(action)) {
                displayData(intent.getStringExtra(BluetoothLeService.EXTRA_DATA));
            } else if (BluetoothLeService.ACTION_GATT_CT_NOT_FOUND.equals(action)) {
                displayData("Characteristics not found...");
            } else if (BluetoothLeService.ACTION_GAT_CT_WRITE_OK.equals(action)) {
                displayData("Write OK");
            } else if (BluetoothLeService.ACTION_GAT_CT_WRITE_FAILED.equals(action)) {
                displayData("Write failed with status:" + intent.getStringExtra(BluetoothLeService.EXTRA_DATA));
            } else if (BluetoothLeService.ACTION_GAT_CT_READ_FAILED.equals(action)) {
                displayData("Read failed with status:" + intent.getStringExtra(BluetoothLeService.EXTRA_DATA));
            }
        }
    };

    @Override
    protected void onDestroy() {
        super.onDestroy();
        bluetoothLeService.close();
        unbindService(mServiceConnection);
        bluetoothLeService = null;
        mBluetoothLeServiceBound = false;
        unregisterReceiver(mGattUpdateReceiver);
    }

    private void connectToDevice(String address) {
        if(mBluetoothLeServiceBound){
            updateConnectingState();
            this.bluetoothLeService.connect(address);
        }else{
            mConnectionState.setText("BLE SERVICE NOT BOUND!");
        }
    }

    private void readSwStatus(){
        Log.i(TAG, "Read sw status...");
        txtData.setText("");
        bluetoothLeService.readDeviceSwStatus();
    }

    private void readDeviceType(){
        Log.i(TAG, "Read device type...");
        txtData.setText("");
        bluetoothLeService.readDeviceType();
    }

    private void writeCharacteristics(){
        Log.i(TAG, "Write...");
        txtData.setText("");
        bluetoothLeService.write();
    }


    @Override
    public void onClick(View view) {
        Log.i(TAG, "Click " + view.getId());
        switch (view.getId()){
            case R.id.btnConnect:
                this.mConnectionState.setText("CONNECTING....");
                this.connectToDevice(this.peripheralAddress);
                break;
            case R.id.btnDisconnect:
                this.mConnectionState.setText("DISCONNECTING...");
                this.bluetoothLeService.disconnect();
                break;
            case R.id.btnReadSwStatus:
                this.readSwStatus();
                break;
            case R.id.btnReadDeviceType:
                this.readDeviceType();
                break;
            case R.id.btnWrite:
                this.writeCharacteristics();
                break;
            default:
                System.out.println("Click listener not implemented!");
                break;
        }
    }

    private static IntentFilter makeGattUpdateIntentFilter() {
        final IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction(BluetoothLeService.ACTION_GATT_CONNECTED);
        intentFilter.addAction(BluetoothLeService.ACTION_GATT_DISCONNECTED);
        intentFilter.addAction(BluetoothLeService.ACTION_GATT_SERVICES_DISCOVERED);
        intentFilter.addAction(BluetoothLeService.ACTION_DATA_AVAILABLE);
        intentFilter.addAction(BluetoothLeService.ACTION_GATT_CT_NOT_FOUND);
        intentFilter.addAction(BluetoothLeService.ACTION_GAT_CT_READ_FAILED);
        intentFilter.addAction(BluetoothLeService.ACTION_GAT_CT_WRITE_OK);
        intentFilter.addAction(BluetoothLeService.ACTION_GAT_CT_WRITE_FAILED);
        return intentFilter;
    }

    private void displayData(String data) {
        if (data != null) {
            txtData.setText(data);
        }
    }

    private void updateConnectedState() {
        mConnectionState.setText("CONNECTED");
        mConnectionState.setTextColor(Color.BLUE);
        mConnected = true;
        this.btnSwStatus.setVisibility(View.VISIBLE);
        this.btnDeviceType.setVisibility(View.VISIBLE);
        this.btnWrite.setVisibility(View.VISIBLE);
        this.txtData.setText("");
    }

    private void updateConnectingState() {
        mConnectionState.setText("CONNECTING...");
        mConnectionState.setTextColor(Color.DKGRAY);
        this.txtData.setText("");
        this.btnSwStatus.setVisibility(View.INVISIBLE);
        this.btnDeviceType.setVisibility(View.INVISIBLE);
        this.btnWrite.setVisibility(View.INVISIBLE);
    }

    private void updateDiscconnectedState() {
        mConnectionState.setText("DISCONNECTED");
        mConnectionState.setTextColor(Color.RED);
        mConnected = false;
        this.btnSwStatus.setVisibility(View.INVISIBLE);
        this.btnDeviceType.setVisibility(View.INVISIBLE);
        this.btnWrite.setVisibility(View.INVISIBLE);
        this.txtData.setText("");
    }
}
